device_mouse_dbclick_enable(false);
globalvar TILE_DEPTH; TILE_DEPTH = 50000;
globalvar MUSICID; MUSICID = -1;
globalvar FULLSCREEN; FULLSCREEN = false;
globalvar MUSIC_SETTING_VOLUME; MUSIC_SETTING_VOLUME = 1;
globalvar SOUND_SETTING_VOLUME; SOUND_SETTING_VOLUME = 1;
globalvar GRAVITY; GRAVITY = 1800;

// Speed & Time Management
globalvar PER_SECOND; PER_SECOND = .01;
globalvar SPD; SPD = 1; // % time dilation
globalvar SLOMO_SECONDS; SLOMO_SECONDS = .01;
globalvar GRAVITY_PER_FRAME; GRAVITY_PER_FRAME = 1;
