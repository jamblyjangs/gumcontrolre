///list_of_maps_save(list,filename)
// Convert each dict to a string and save
var tIdx_bpv ;
var fh_bpv = file_text_open_write(argument1);
for(tIdx_bpv=0;tIdx_bpv<ds_list_size(argument0);tIdx_bpv++){
    var as_string_bpv = ds_map_write(ds_list_find_value(argument0,tIdx_bpv)) ;
    file_text_write_string(fh_bpv,as_string_bpv+chr(10));
}
file_text_close(fh_bpv);
