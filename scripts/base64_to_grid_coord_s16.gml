/// base64_to_grid_coord_s16(base64_string)
//  WARNING: no longer does what it used to (and what its name implies)
//  converts a u32 into x,y coords
buffer_seek (BUFFER_4BYTE,buffer_seek_start,0);
buffer_write(BUFFER_4BYTE,buffer_s32,argument0);
buffer_seek(BUFFER_4BYTE,buffer_seek_start,0);
var xy = 0;
xy[1]   = buffer_read(BUFFER_4BYTE,buffer_s16);
xy[0]   = buffer_read(BUFFER_4BYTE,buffer_s16);
return xy;
