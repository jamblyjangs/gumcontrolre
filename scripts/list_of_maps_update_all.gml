///list_of_maps_update_all(list,key,value)
// For all maps in list, set to valuy where key=='key'
if(ds_exists(argument0,ds_type_list))
{
    var tIdx_plo ;
    for(tIdx_plo=0;tIdx_plo<ds_list_size(argument0);tIdx_plo++){
        var thisDict_plo = ds_list_find_value(argument0,tIdx_plo) ;
        if( ds_exists(thisDict_plo,ds_type_map))
        {
            ds_map_replace( thisDict_plo, argument1, argument2);
        }
    }
    return true ;
}

return false;
