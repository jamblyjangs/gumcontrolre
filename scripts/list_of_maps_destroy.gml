/// list_of_maps_destroy(target_list)
if(ds_exists(argument0,ds_type_list))
{
    var i_bs0b;
    for(i_bs0b=0;i_bs0b<ds_list_size(argument0);i_bs0b++){
        var theDict_bs0b = ds_list_find_value(argument0,i_bs0b);
        if(ds_exists(theDict_bs0b,ds_type_map)){ ds_map_destroy(theDict_bs0b); }
    }
    ds_list_destroy(argument0);
}
