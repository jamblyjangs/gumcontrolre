// Sets the resolution, then goes to the target room.
dispH = display_get_height();
dispW = display_get_width();

view_width = min(1600,dispW-100);
wh_ratio = dispW/dispH;
view_height = ceil(view_width/wh_ratio);

globalvar GUIWIDTH, GUIHEIGHT;
GUIWIDTH = view_width;
GUIHEIGHT = view_height;
display_set_gui_size(view_width,view_height)

var i;
for (i = 0; i < 500; i += 1){    
    if (room_exists(i)){        
        room_set_view(i, 0, 1, 0, 0, view_width, view_height, 0, 0, view_width, view_height, -1, -1, -1, -1, noone)                       
    }
}

if window_get_fullscreen() {
    window_set_size(display_get_width(), display_get_height())
    window_set_position(0, 0)
    surface_resize(application_surface, view_width, view_height);    
}
else {
    window_set_size(view_width, view_height)
    window_set_position((dispW-GUIWIDTH)/2,(dispH-GUIHEIGHT)/2)
    surface_resize(application_surface, view_width, view_height); 
}
