/// draw_text_to_fit_oneline(string,pixelwidth,pixelheight,xdraw,ydraw,minscale,maxscale,color,drop_pixels[,alpha])

var txt = argument[0];
var pixelwidth = argument[1];
var pixelheight = argument[2];
var xdraw = argument[3],
var ydraw = argument[4];
var minscale = argument[5];
var maxscale = argument[6];
var color = argument[7];
var drop_pixels = argument[8];
var alpha = 1;

if argument_count > 9 {
    alpha = argument[9];
}
var tscale = 1;
var theight = max(1,string_height(txt));
var twidth = max(1,string_width(txt));

tscale = clamp( min(pixelwidth/twidth, pixelheight/theight), minscale, maxscale);

if drop_pixels > 0 {
    shadowtext(xdraw,ydraw,txt,tscale,tscale,0,color,alpha,drop_pixels);
}
else draw_text_transformed_colour(xdraw,ydraw,txt,tscale,tscale,0,color,color,color,color,alpha)
return tscale;
