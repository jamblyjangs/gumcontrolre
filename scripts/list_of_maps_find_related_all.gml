///list_of_maps_find_related_all(list,key1,value1,key2)
// Find the values from all dicts at key2 where key1=>value1
var tIdx ;
var out_list = ds_list_create() ;
if(ds_exists(argument0,ds_type_list))
{
    for(tIdx=0;tIdx<ds_list_size(argument0);tIdx++){
        var thisDict = ds_list_find_value(argument0,tIdx) ;
        if(ds_exists(thisDict,ds_type_map))
        {
            var value = ds_map_find_value(thisDict,argument1) ;
            if(not is_undefined(value))
            {
                if(value==argument2){
                    var value2 = ds_map_find_value(thisDict,argument3) ;
                    if( not is_undefined(value2) )
                    {
                        ds_list_add(out_list, value2) ;
                    }
                }
            }
        }
    }
}
return out_list ;
