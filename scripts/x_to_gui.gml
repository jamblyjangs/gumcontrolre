/// x_to_gui(xx=x)
var xx = x;
if argument_count > 0
    xx = argument[0];

return GUIWIDTH*(xx-view_xview)/view_wview;
