/// play_sound_gv(sound,priority,looping)

// Plays sound effect with global volume attached.

var sid = audio_play_sound(argument0,argument1,argument2);
audio_sound_gain(sid,1,0);
return sid;
