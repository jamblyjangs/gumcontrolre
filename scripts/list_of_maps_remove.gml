///list_of_maps_remove(list,key,value)
// Remove the map from the list for which map[key]==value
var tIdx_n9sy ;
for(tIdx_n9sy=0;tIdx_n9sy<ds_list_size(argument0);tIdx_n9sy++)
{
    var value_n9sy = ds_map_find_value(ds_list_find_value(argument0,tIdx_n9sy),argument1) ;
    if( not is_undefined(value_n9sy) )
    {
        if( value_n9sy == argument2 )
        {
            ds_map_destroy(argument0[|tIdx_n9sy]);
            ds_list_delete(argument0,tIdx_n9sy);
            return true ;
        }
    }
}
return false ;
