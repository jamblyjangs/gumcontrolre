///list_of_maps_find_all(list,key)
// Find the values from all dicts at key
var tIdx_b8s6 ;
var out_list_b8s6 = ds_list_create() ;
if(ds_exists(argument0,ds_type_list))
{
    for(tIdx_b8s6=0;tIdx_b8s6<ds_list_size(argument0);tIdx_b8s6++){
        var thisDict_b8s6 = ds_list_find_value(argument0,tIdx_b8s6) ;
        if(ds_exists(thisDict_b8s6,ds_type_map))
        {
            var value_b8s6 = ds_map_find_value(thisDict_b8s6,argument1) ;
            if(not is_undefined(value_b8s6))
            {
                ds_list_add(out_list_b8s6, value_b8s6) ;
            }
        }
    }
}
return out_list_b8s6 ;
