/// shadowtext_ext(x,y,string,spacing,width,xscale,yscale,angle,color,alpha[,drop_pixels])

var xx = argument[0];
var yy = argument[1];
var txt = argument[2];
var tspacing = argument[3];
var w = argument[4];
var xscale = argument[5];
var yscale = argument[6];
var angle = argument[7];
var color = argument[8];
var alpha = argument[9];
var drop_pixels = 2;
if argument_count > 10 {
    drop_pixels = argument[10];
}

var bottomcol = color_change(color,.3);
draw_text_ext_transformed_colour(xx,ceil(yy+drop_pixels*.5),txt,tspacing,w,
            xscale,yscale,angle,
            bottomcol,bottomcol,bottomcol,bottomcol,alpha)
draw_text_ext_transformed_colour(xx,ceil(yy-drop_pixels*.5),txt,tspacing,w,
            xscale,yscale,angle,
            color,color,color,color,alpha)
