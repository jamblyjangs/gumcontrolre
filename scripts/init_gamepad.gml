globalvar CURSOR_LAST_X;    CURSOR_LAST_X       = -1;
globalvar CURSOR_LAST_Y;    CURSOR_LAST_Y       = -1;
globalvar GAMEPAD_DEADZONE; GAMEPAD_DEADZONE    = .2;
globalvar CURRENT_GAMEPAD;  CURRENT_GAMEPAD     = -1;
globalvar GAMEPAD_CONFIG;   GAMEPAD_CONFIG      = 0;
globalvar BSID_CURSOR_SPRITE; BSID_CURSOR_SPRITE = -1;

globalvar GAMEPAD_KEYMAP; GAMEPAD_KEYMAP = ds_map_create();
ds_map_add(GAMEPAD_KEYMAP,gp_face1,gp_face2);
ds_map_add(GAMEPAD_KEYMAP,gp_face3,gp_face1);
ds_map_add(GAMEPAD_KEYMAP,gp_face2,gp_face3);
ds_map_add(GAMEPAD_KEYMAP,gp_face4,gp_face4);
