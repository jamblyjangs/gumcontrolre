///list_of_maps_find_related(list,key1,value1,key2 [,default])
// Find the value from a list of dicts at key2, where key1=value1 in that same dict
var tIdx_g4a ;
if(ds_exists(argument[0],ds_type_list))
{
    var list_g4a = argument[0] ;
    for(tIdx_g4a=0;tIdx_g4a<ds_list_size(list_g4a);tIdx_g4a++){
        var thisDict_g4a = list_g4a[|tIdx_g4a];
        if( ds_exists(thisDict_g4a,ds_type_map) )
        {
            var value_g4a = ds_map_find_value(thisDict_g4a,argument[1]) ;
            if( not is_undefined(value_g4a))
            {
                if( value_g4a == argument[2]){
                    var value2_g4a = ds_map_find_value(thisDict_g4a,argument[3]) ;
                    if(not is_undefined(value2_g4a)){return value2_g4a;}
                    break; 
                }
            }
        }
    }
}
if(argument_count==5){ return argument[4];}
else{ return '' ; }
