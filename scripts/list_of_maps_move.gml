///list_of_maps_move(from_list,key,value,to_list)
// Move the map from the list for which map[key]==value to a different list
var tIdx_bows8 ;
for(tIdx_bows8=0;tIdx_bows8<ds_list_size(argument0);tIdx_bows8++){
    var value_bows8 = ds_map_find_value(ds_list_find_value(argument0,tIdx_bows8),argument1) ;
    if( value_bows8 == argument2 ){
        // Move this map over to the to_list
        ds_list_add(argument3,ds_list_find_value(argument0,tIdx_bows8));
        ds_list_delete(argument0,tIdx_bows8);
        return true ;
    }
}
return false ;
