///list_of_maps_update(list,key1,value1,key2,value2)
// Update the map value from the list for which map[key1]==value1 to set map[key2]==value2
if(ds_exists(argument0,ds_type_list))
{
    var tIdx_plo ;
    for(tIdx_plo=0;tIdx_plo<ds_list_size(argument0);tIdx_plo++){
        var thisDict_plo = ds_list_find_value(argument0,tIdx_plo) ;
        if( ds_exists(thisDict_plo,ds_type_map))
        {
            var value_plo = ds_map_find_value(thisDict_plo,argument1) ;
            if( not is_undefined(value_plo) )
            {
                if( value_plo == argument2 ){
                    ds_map_replace(thisDict_plo,argument3,argument4);
                    return true ;
                }
            }
        }
    }
}

return false;
