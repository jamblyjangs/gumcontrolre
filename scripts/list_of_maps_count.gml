///list_of_maps_count(list,key,value)
// Tally the maps for which map[key]==value
if(ds_exists(argument0,ds_type_list))
{
    var tIdx_ncs7 ;
    var count_ncs7 = 0 ;
    for(tIdx_ncs7=0;tIdx_ncs7<ds_list_size(argument0);tIdx_ncs7++){
        var value_ncs7 = ds_map_find_value(ds_list_find_value(argument0,tIdx_ncs7),argument1) ;
        if( value_ncs7 == argument2 ){
            count_ncs7++ ;
        }
    }
    return count_ncs7 ;
}
