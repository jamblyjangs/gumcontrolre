///list_of_maps_sort(list_of_maps,key[,cast_as_real=false])
// sorts the list in-place (does not return anything)
// uses insertion-sorting (slow, only good for small lists)
var cast_as_real_hfp = false;
if(argument_count==3){cast_as_real_hfp = argument[2];}
if(!ds_exists(argument[0],ds_type_list)){ return false; }
if(ds_list_size(argument[0])>1)
{
    var list_copy_hfp = ds_list_create() ;
    var the_list_hfp  = argument[0] ;
    ds_list_copy(list_copy_hfp,the_list_hfp) ;
    ds_list_clear(the_list_hfp);
    
    // Move the first map over
    ds_list_add(the_list_hfp,list_copy_hfp[|0]);
    ds_list_delete(list_copy_hfp,0);
    
    var sourceIdx_hfp ;
    var targetIdx_hfp ;
    for(sourceIdx_hfp=ds_list_size(list_copy_hfp)-1;sourceIdx_hfp>-1;sourceIdx_hfp--)
    {
        var sourceDict_hfp = list_copy_hfp[|sourceIdx_hfp];
        if(!ds_exists(sourceDict_hfp,ds_type_map)){
            ds_list_delete(list_copy_hfp,sourceIdx_hfp);
            continue ;
        }
        var source_value   = sourceDict_hfp[?argument[1]] ;
        if(is_undefined(source_value)){
            ds_map_destroy(sourceDict_hfp);
            ds_list_delete(list_copy_hfp,sourceIdx_hfp);
            continue;
        }
        ds_list_delete(list_copy_hfp,sourceIdx_hfp);
        // Move this dict into the target
        for(targetIdx_hfp=0;targetIdx_hfp<ds_list_size(the_list_hfp);targetIdx_hfp++)
        {
            var targetDict_hfp = the_list_hfp[|targetIdx_hfp];
            var target_value   = targetDict_hfp[?argument[1]] ;
            // if the source is <= the target, insert it at this index
            var source_is_lesser = false;
            if(cast_as_real_hfp){ source_is_lesser = real(source_value) <= real(target_value);}
            else{source_is_lesser=source_value <= target_value;}
            if(source_is_lesser)
            {
                ds_list_insert(the_list_hfp,targetIdx_hfp,sourceDict_hfp);
                break ;
            }
            else if( targetIdx_hfp==(ds_list_size(the_list_hfp)-1))
            {
                ds_list_add(the_list_hfp,sourceDict_hfp);
                break;
            }
        }
    }
}
