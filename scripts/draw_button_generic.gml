/// draw_button_generic(xx,yy,width,height,radius,text,text_scale,scale,color)

var xx = argument0; // X draw position
var yy = argument1; // Y draw position
var wd = argument2; // Width
var ht = argument3; // Height
var rad = argument4; // Radius
var txt = argument5; // Text to draw
var tscale = argument6; // Text scale
var sc = argument7; // Scale
var col = argument8; // Color

var dark_c = color_change(col,.4);

if wd > 0 && ht > 0 {
    wd *= sc;
    ht *= sc;
    draw_set_alpha(.8);
    draw_rectangle_colour(xx-wd*.5, yy-ht*.5, xx+wd*.5, yy+ht*.5,
        c_black,c_black,c_black,c_black,0);
    draw_set_alpha(1);
    draw_rectangle_colour(xx-wd*.5, yy-ht*.5, xx+wd*.5, yy+ht*.5,
        dark_c,dark_c,dark_c,dark_c,1);
}    
if rad > 0 {
    rad *= sc;
    draw_set_alpha(.8);
    draw_circle_colour(xx, yy, rad, c_black, c_black, 0);
    draw_set_alpha(1);
    draw_circle_colour(xx, yy, rad, dark_c, dark_c, 1);
}

if txt != '' {
    font_set(fnt_reg,fa_center,fa_middle);
    var tfit = max(width,radius*2);
    var theight = max(height,radius*2);
    draw_text_to_fit_oneline(txt,(tfit-10)*sc,(theight-10)*sc,xx,yy,.01*sc,tscale*sc,col,0);
    //shadowtext(xx,yy,txt,tscale*sc,tscale*sc,0,col,1,2);
}
