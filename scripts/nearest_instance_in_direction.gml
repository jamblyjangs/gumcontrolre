/// nearest_instance_in_direction(direction,dir_range,object_type)
var dir = argument0;
var dir_range = argument1; // Amount of degrees to vary in either direction
var itype = argument2;

var curdist = 50000;
var me = id;

var instance_found = noone;
with itype {
    if id != me {
        var dirdist = 0;
        dirdist = get_dirdist(me.x,me.y,x,y);
        if abs(angle_diff(dirdist[0],dir)) <= dir_range {
            if dirdist[1] < curdist {
                curdist = dirdist[1];
                instance_found = id;
            }
        }
    }
}

return instance_found;
