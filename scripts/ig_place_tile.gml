/// ig_place_tile(grid_x,grid_y)
var xg = argument0;
var yg = argument1;

var tileX,Yoffset,newTile,colormod,darkness,tileset,tileChosen,tilecolor = c_white;
var gridtile_xoffset = IG_GRIDTILEWIDTH*.5;

Yoffset = irandom_range(-3,3);
Xoffset = irandom_range(-5,5);            

tileset = bg_ig_tile;         
tileChosen = IG_GRIDTILEWIDTH*irandom(round(background_get_width(tileset)/IG_GRIDTILEWIDTH)-1)

colormod = color_change( c_white,1 - random(.15) );
var tile_xspawn = ig_grid_pos_to_x(xg,yg)-gridtile_xoffset+Xoffset;
var tile_yspawn = ig_grid_pos_to_y(xg,yg)-IG_GRIDTILE_YOFFSET+Yoffset;
var tile_depth = -ig_grid_pos_to_y(xg,yg)+TILE_DEPTH;
newTile = tile_add(tileset,tileChosen,0,IG_GRIDTILEWIDTH,background_get_height(tileset),tile_xspawn,tile_yspawn,tile_depth)
tile_set_blend(newTile,colormod)
