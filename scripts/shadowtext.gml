/// shadowtext(x,y,string,xscale,yscale,angle,color,alpha,drop_pixels)

var xx = argument[0];
var yy = argument[1];
var txt = argument[2];
var xsc = argument[3];
var ysc = argument[4];
var angle = argument[5];
var color = argument[6];
var shadowcol = color_change(color,.35);
var alpha = argument[7];
var drop_pixels = 2;
if argument_count > 8
    drop_pixels = argument[8];
draw_text_transformed_colour(xx,yy+drop_pixels,txt,xsc,ysc,angle,shadowcol,shadowcol,shadowcol,shadowcol,alpha)
draw_text_transformed_colour(xx,yy,txt,xsc,ysc,angle,color,color,color,color,alpha)
